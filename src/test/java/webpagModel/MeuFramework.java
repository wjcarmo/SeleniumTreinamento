package webpagModel;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

import org.codehaus.plexus.component.configurator.converters.basic.UriConverter;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class MeuFramework  extends EnumBrowser{
	
	
	  public WebDriver driver;
	  BrowserType browser;
	  private final int tempoDeEspera=10;
  
	  
	  public MeuFramework(BrowserType browser){		
		  this.browser = browser;
	  }
	 	  
	  public MeuFramework(){ 
	  }
	  
	  @Before
	  public void inicialize() throws MalformedURLException
	  {
		  beforeTest(browser);
	  }
	  public void beforeTestWindows() 
	  {
		  
	 
		  if(browser == BrowserType.Firefox) {
			  
			  System.setProperty("webdriver.gecko.driver", "D:\\Selenium370\\Driver\\geckodriver.exe");		 
			  driver = new FirefoxDriver();		 
		 
		  }else if (browser == BrowserType.IE) { 
		 
		     System.setProperty("webdriver.ie.driver", "D:\\Selenium370\\Driver\\IEDriverServer.exe");			 
			  driver = new InternetExplorerDriver();
		 
		  } else if (browser == BrowserType.Chrome) { 
			  
			  System.setProperty("webdriver.chrome.driver", "D:\\Selenium370\\Driver\\chromedriver.exe");			  
			  driver = new ChromeDriver( );	
		  
		  }else if (browser == BrowserType.Edge) { 
			  
			  System.setProperty("webdriver.edge.driver", "D:\\Selenium370\\Driver\\MicrosoftWebDriver.exe");	  
			  driver = new EdgeDriver();			  
		  }
		  
		  driver.manage().deleteAllCookies();
		  driver.manage().window().maximize();
		  
	 		 	 
	  }
	 	 

	  public void beforeTest(BrowserType browser) throws MalformedURLException  
	  {
		  	 
		  if(browser == BrowserType.Firefox) {
			  
			  DesiredCapabilities cap = DesiredCapabilities.firefox();
			  
			  cap.setCapability("version","");
			  cap.setCapability("platform","LINUX");
			  
			  driver = new RemoteWebDriver(new  URL("http://192.168.99.100:4446/wd/hub"),cap);
	
		 
		  } else if (browser == BrowserType.Chrome) { 
			  
			  DesiredCapabilities cap = DesiredCapabilities.chrome();
			  
			  cap.setCapability("version","");
			  cap.setCapability("platform","LINUX");
			  
			  driver = new RemoteWebDriver(new  URL("http://192.168.99.100:4446/wd/hub"),cap);
				  
		  }
		  
		  driver.manage().deleteAllCookies();
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait(tempoDeEspera, TimeUnit.SECONDS);
		  
	 		 	 
	  }
	 
	  @After 
	  public void afterTest() {
	 
			driver.quit();
	 
		}
	 
	}
