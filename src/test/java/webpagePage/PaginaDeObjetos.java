package webpagePage;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class PaginaDeObjetos {
	
	private WebDriver driver;
	protected String siteParamentro;
	
	
	@FindBy(how = How.LINK_TEXT, using = "gb_70") 	 private WebElement BotaoFazerLogin;	
	@FindBy(how = How.ID,  using = "identifierId")	 private WebElement EmailDoUsuario;
	@FindBy(how = How.CLASS_NAME, using = "CwaK9")   private WebElement BotaoProximo;
	@FindBy(how = How.NAME, using = "password")      private WebElement SenhaDoUsuario;
	@FindBy(how = How.ID, using = "lst-ib")          private WebElement InformaDados;
	@FindBy(how = How.ID, using = "source")          private WebElement TextoParaTraduzir;
	
	
	public PaginaDeObjetos(WebDriver driver)
	{
		this.driver = driver;
	}
	
	
	public void InformeLinkDoSite(String siteParamentro) throws InterruptedException 
	{

		this.driver.manage().window().maximize();		
		this.driver.get(siteParamentro);
		
		PageFactory.initElements(this.driver, this); 
		this.siteParamentro = siteParamentro;
		Thread.sleep(1000);
	
	}
		 
	  
	  public void ChamadaPorBrowserSelenium( WebDriver driver) throws InterruptedException {
		  
		 
		  driver.get("http://www.google.com.br");
		  driver.findElement(By.id("lst-ib")).sendKeys("Selenium 3.0");
		  
		  Thread.sleep(1000);	 
		 	  	  
		  Assert.assertEquals("Wenderson", "Wenderson");	
		  driver.quit();
	  
	  }
	  
	  public void DigitaDadosParaPesquisar(String dadosParaPesquisar)
	  {
		  InformaDados.sendKeys(dadosParaPesquisar);
	  }
	  
	  public void DigitaTextoParaTraduzir(String dadosParaTraduzir)
	  {
		  TextoParaTraduzir.sendKeys(dadosParaTraduzir);
	  }
	
	
	

}
