package webpageObject;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import webpagModel.MeuFramework;
import webpagePage.PaginaDeObjetos;


public class TesteSeleniumContannierChrome extends MeuFramework
{
	
	PaginaDeObjetos PaginaEmTestes;	
	
	  
	public TesteSeleniumContannierChrome()
	{
		super(BrowserType.Chrome);
	}
	
  @Test
  public void ChamadaChromeSeleniumContainer() throws InterruptedException {
	  	
		  
	  PaginaEmTestes = new PaginaDeObjetos(driver);	 	    	  
	  PaginaEmTestes.InformeLinkDoSite("https://translate.google.com.br/");
	  PaginaEmTestes.DigitaTextoParaTraduzir("I am CHROME Browser");	 
	    
  }
 

  public void ChamadaChromeSelenium() throws InterruptedException {
	  
	  System.setProperty("webdriver.chrome.driver", "D:\\Selenium370\\Driver\\chromedriver.exe");	  
	  WebDriver driver = new ChromeDriver( );	  
	  ChamadaPorBrowserSelenium(driver);  
  }
  public void ChamadaEdgeSelenium() throws InterruptedException {
	  	  
	  System.setProperty("webdriver.edge.driver", "D:\\Selenium370\\Driver\\MicrosoftWebDriver.exe");	  
	  WebDriver driver = new EdgeDriver();		  
	  ChamadaPorBrowserSelenium(driver); 
  }
  public void ChamadaInternetExplorerSelenium() throws InterruptedException {
	  
	  System.setProperty("webdriver.ie.driver", "D:\\Selenium370\\Driver\\IEDriverServer.exe");	  
	  WebDriver driver = new InternetExplorerDriver();	  
	  ChamadaPorBrowserSelenium(driver);  
  }
  public void ChamadaOperaComSelenium() throws InterruptedException {
	   
  }
  public void ChamadaPorBrowserSelenium( WebDriver driver) throws InterruptedException {
	  
	 
	  driver.get("http://www.google.com.br");
	  driver.findElement(By.id("lst-ib")).sendKeys("Unisys do Brasil");
	  
	  Thread.sleep(1000);	 
	 	  	  
	  Assert.assertEquals("Wenderson", "Wenderson");	
	  driver.quit();
  
  }
  
}

